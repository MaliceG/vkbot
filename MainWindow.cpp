#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QtNetwork>
#include <cmath>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    VKClient = NULL;
    connect(ui->pbSwitchServer, &QPushButton::clicked, this, &MainWindow::switchServer);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendHelp(int messagePeer)
{
    QString helpString = "Доступные команды:";
    helpString += "\n!help";
    helpString += "\n!было?";
    helpString += "\n!unix";

    VKClient->getMessages()->sendMessage(messagePeer, helpString);
}

void MainWindow::findArt(int messageID, int messagePeer, QStringList messageAttach)
{
    if (messageAttach.isEmpty())
    {
        VKClient->getMessages()->sendMessage(messagePeer, "Нет арта", messageID);
        return;
    }

//    qDebug() << messageAttach.first();
    if (messageAttach.first().split(";")[0] != "photo")
    {
        VKClient->getMessages()->sendMessage(messagePeer, "Это не арт", messageID);
        return;
    }

    int photoID = messageAttach.first().split(";")[1].split("_")[1].toInt();
//    qDebug() << "Photo ID: " << photoID;

    //это нужно, чтобы получить url на фотографию в аттаче, т.к. при появлении сообщения фотография еще не успевает попасть в attachesHistory
    QVariantMap photoInfo; //photo_75 - small photo url, height, width, id
    {
        //локальное пространство для временных переменных
        QVariantMap response =  QtJson::parse( VKClient->getMessages()->getById(messageID)).toMap()["response"].toMap();
        QVariantList responseItems = response["items"].toList();
        photoInfo = responseItems.first().toMap()["attachments"].toList().first().toMap()["photo"].toMap();
    }

    //проверка, получил ли я нужное фото
    if (photoInfo["id"].toInt() != photoID)
    {
        VKClient->getMessages()->sendMessage(messagePeer, "ID найденного фото не совпадает с искомым", messageID);
        return;
    }

//    qDebug() << "My photo size: " << photoInfo["width"].toString() << photoInfo["height"].toString();

    //TODO: по умолчанию искать за 6 месяцев, либо искать по пользовательскому интервалу
    int minusMonthTime = QDateTime::currentDateTime().addMonths(-6).toTime_t();
    bool isFounded = false;
    int foundedMessageID;
    int foundedDate;
    QString currentTime = QString::number(QDateTime::currentDateTime().toTime_t());
    while (true && !isFounded)
    {
        QVariantMap response = QtJson::parse(VKClient->getMessages()->getAttachesHistory(messagePeer, "photo", currentTime)).toMap()["response"].toMap();
        //если больше не получаем аттачи, значит нужное фото не найдено, выходим из цикла
        if (response["items"].toList().count() == 0)
            break;

        /*
         * тут происходит проверка, поиск и сравнение фото по id и размеру
         * если размер фото соответсвует искомому, то берется наименьший размер изображения
         * и сравнивается каждый пиксель двух изображений
         */

        QVariantList allAttaches = response["items"].toList();
        foreach (QVariant curAttach, allAttaches)
        {
//            qDebug() << "Current attach:" << curAttach;
            QVariantMap curPhoto = curAttach.toMap()["attachment"].toMap()["photo"].toMap();
//            qDebug() << minusMonthTime << curPhoto["date"].toInt();
            if (minusMonthTime > curPhoto["date"].toInt())
            {
                //на этом моменте заканчиваем поиск
                VKClient->getMessages()->sendMessage(messagePeer, "Фото не найдено", messageID);
                return;
            }

            //сравниваю размеры фото, если они совпадают, тогда сравниваю фото более детально
            if (
                    photoInfo["width"].toInt() == curPhoto["width"].toInt() &&
                    photoInfo["height"].toInt() == curPhoto["height"].toInt() &&
                    photoInfo["id"].toInt() != curPhoto["id"].toInt()
                )
            {
                isFounded = this->isPhotoTheSame(photoInfo["photo_75"].toString(), curPhoto["photo_75"].toString());
//                VKClient->getMessages()->sendMessage(messagePeer, "Нужно сравнить:\n" + photoInfo["photo_75"].toString() + "\n" + curPhoto["photo_75"].toString(), messageID);
                qDebug() << "Нужно сравнить:\n" + photoInfo["photo_75"].toString() + "\n" + curPhoto["photo_75"].toString();
                if (isFounded)
                {
                    foundedDate = curPhoto["date"].toInt();
                    foundedMessageID = curAttach.toMap()["message_id"].toInt();
                    break;
                }
            }
        }

        //если запрос не вернул поле next_from, тогда заканчиваем поиск
        if (response.keys().contains("next_from"))
            currentTime = response["next_from"].toString();
        else break;
    }

    if (isFounded)
        VKClient->getMessages()->sendMessage(messagePeer, "Уже был " + QDateTime::fromTime_t(foundedDate).toString("dd.MM.yyyy hh:mm:ss"), messageID);
    else
        VKClient->getMessages()->sendMessage(messagePeer, "Это фото не встречалось ранее", messageID);


}

void MainWindow::sendUnixTime(int messageID, int messagePeer, QString messageText)
{
    QStringList parts = messageText.split(" ");
    if (parts.count() < 2)
    {
        VKClient->getMessages()->sendMessage(messagePeer, "не указано время", messageID);
        return;
    }

    QString timeString;
    QDateTime datetime = QDateTime::fromString(parts.at(1), QString("dd.MM.yyyy"));
    if (datetime.isValid())
        timeString = QString::number(datetime.toTime_t());
    else
        timeString = "Укажите валидную дату формата \"дд.мм.гггг\"";

    VKClient->getMessages()->sendMessage(messagePeer, timeString, messageID);
}

bool MainWindow::isPhotoTheSame(QString url1, QString url2)
{
    QPixmap pixmap1, pixmap2;

    QNetworkAccessManager *nam = new QNetworkAccessManager(this);
    QNetworkRequest request = QNetworkRequest(QUrl(url1));
    QEventLoop loop;
    connect(nam, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

    QNetworkReply *reply = nam->get(request);
    loop.exec();
    pixmap1.loadFromData(reply->readAll());

    request.setUrl(QUrl(url2));
    reply = nam->get(request);
    loop.exec();
    pixmap2.loadFromData(reply->readAll());

    reply->deleteLater();
    nam->deleteLater();

    QImage image1 = pixmap1.toImage();
    QImage image2 = pixmap2.toImage();
    qDebug() << image1.size() << image2.size();

    if (image1.size() != image2.size())
        return false;

    for (int i = 0; i < pixmap1.width(); i++)
        for (int j = 0; j < pixmap1.height(); j++)
            if (image1.pixel(i,j) != image2.pixel(i,j))
            {
                //делаю допустимую ошибку в цвете на 5%
                QColor color1 = image1.pixelColor(i,j);
                QColor color2 = image2.pixelColor(i,j);
//                qDebug() << i << j << image1.pixelColor(i,j) << image2.pixelColor(i,j);

                double d = sqrt(sqr(color2.red() - color1.red()) + sqr(color2.green() - color1.green()) + sqr(color2.blue() - color1.blue()));
                double percent = d / sqrt( sqr(255) + sqr(255) + sqr(255) );
//                qDebug() << percent;

                if (percent > 0.05)
                    return false;
            }

    return true;
}

void MainWindow::switchServer()
{
    if (!VKClient)
        VKClient = new VK(ui->leAccessToken->text(), this);

    MessageListener *msgListener = VKClient->getMessageListener();
    if (!msgListener->isWorking())
    {
        ui->pbSwitchServer->setText("Остановить сервер");
        msgListener->startLongPollServer();
        connect(msgListener, SIGNAL(eventGetMessage(int,QVector<int>,int,QString,QStringList)), this, SLOT(processMessage(int,QVector<int>,int,QString,QStringList)));
    }
    else
    {
        ui->pbSwitchServer->setText("Запустить сервер");
        msgListener->stopLongPollServer();
        disconnect(msgListener, SIGNAL(eventGetMessage(int,QVector<int>,int,QString,QStringList)), this, SLOT(processMessage(int,QVector<int>,int,QString,QStringList)));
    }

}

void MainWindow::processMessage(int messageID, QVector<int> messageFlags, int messagePeer, QString messageText, QStringList messageAttachs)
{
    qDebug() << messageID << messageText << messagePeer;
    qDebug() << messageAttachs;

    if (messageText.startsWith("!help")) sendHelp(messagePeer);
    if (messageText.contains(QRegExp(".*был(о|а).*"))) findArt(messageID, messagePeer, messageAttachs);
    if (messageText.startsWith("!unix")) sendUnixTime(messageID, messagePeer, messageText);
}
