#include "UrlSimplifier.h"
#include <QEventLoop>

UrlSimplifier::UrlSimplifier(QString url, QObject *parent) : QObject(parent)
{
    networkAccessManager = new QNetworkAccessManager(this);

    this->url = QUrl(url);
}

void UrlSimplifier::setUrl(QString url)
{
    this->url.setUrl(url);
}

QString UrlSimplifier::getUrl()
{
    return url.toString();
}

void UrlSimplifier::setQueryData(QString key, QString data)
{
    if (!urlQuery.queryItemValue(key).isEmpty())
        urlQuery.removeQueryItem(key);
    urlQuery.addQueryItem(key, data);
}

void UrlSimplifier::clearQueryData()
{
    urlQuery.clear();
}

QByteArray UrlSimplifier::get()
{
    url.setQuery(urlQuery);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );
    QNetworkReply *reply = networkAccessManager->get(request);

    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    QByteArray response = reply->readAll();
    reply->deleteLater();

    return response;
}

void UrlSimplifier::getNoLock()
{
    url.setQuery(urlQuery);
    QNetworkRequest *request = new QNetworkRequest(url);
    request->setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );
    QNetworkReply *reply = networkAccessManager->get(*request);
    connect(reply, &QNetworkReply::finished, [=]()
    {
        emit finished(reply->readAll());
        reply->deleteLater();
    });
}
