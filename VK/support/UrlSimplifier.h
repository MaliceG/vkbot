#ifndef URLSIMPLIFIER_H
#define URLSIMPLIFIER_H

#include <QObject>
#include <QtNetwork>

class UrlSimplifier : public QObject
{
    Q_OBJECT
public:
    explicit UrlSimplifier(QString url, QObject *parent = nullptr);

    void setUrl(QString url);
    QString getUrl();

    void setQueryData(QString key, QString data); //добавление передаваемых параметров
    void clearQueryData(); //очистка всех параметров

    QByteArray get(); //ждет пока получит все данные
    void getNoLock(); //не ждет получения данных, по завершению эмитирует сигнал finished(QByteArray)

private:
    QNetworkAccessManager *networkAccessManager;
    QUrl url;
    QUrlQuery urlQuery;

signals:
    void finished(QByteArray);

public slots:
};

#endif // URLSIMPLIFIER_H
