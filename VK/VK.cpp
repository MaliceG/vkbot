#include "VK.h"

VK::VK(QString accessToken, QObject *parent) : QObject(parent)
{
    _accessToken = accessToken;
    messageListener = new MessageListener(_accessToken, this);
    user = new User(_accessToken, this);
    messages = new Messages(_accessToken, this);

    //    urlSimplifier = new UrlSimplifier(this);
}

MessageListener *VK::getMessageListener()
{
    return this->messageListener;
}

Messages *VK::getMessages()
{
    return this->messages;
}
