#include "Messages.h"

Messages::Messages(QString accessToken, QObject *parent) : QObject(parent)
{
    _accessToken = accessToken;
    basicUrl = "https://api.vk.com/method/messages.";

    requester = new UrlSimplifier("https://api.vk.com/method/messages.", this);
    requester->setQueryData("access_token", _accessToken);
    requester->setQueryData("v", "5.73");

}

void Messages::sendMessage(int peerID, QString message, int messageID)
{
    QMap<QString, QString> params;
    params["user_id"] = QString::number(peerID);
    params["message"] = message;
    if (messageID != -1) params["forward_messages"] = QString::number(messageID);

    this->sendMethod("send", params);
}

QByteArray Messages::getAttachesHistory(int chatID, QString mediaType, QString timeStart)
{
    QMap <QString, QString> params;
    params["peer_id"] = QString::number(chatID);
    params["media_type"] = mediaType;
    params["count"] = "200";
    params["start_from"] = timeStart;
//    params["photo_sizes"] = "1"; //возвращает по детальную информацию о каждом размере

    return sendMethod("getHistoryAttachments", params);
}

QByteArray Messages::getById(int messageID)
{
    QMap<QString, QString> params;
    params["message_ids"] = QString::number(messageID);

    return sendMethod("getById", params);
}

QByteArray Messages::sendMethod(QString methodName, QMap<QString, QString> params)
{
    QThread::msleep(350);
    requester->setUrl(basicUrl + methodName);
    QStringList keys = params.keys();
    for (int i = 0; i < keys.count(); i++)
        requester->setQueryData(keys[i], params.value(keys[i]));
    return requester->get();
}
