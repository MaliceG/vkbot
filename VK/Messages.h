#ifndef MESSAGES_H
#define MESSAGES_H

#include <QObject>
#include "support/UrlSimplifier.h"

class Messages : public QObject
{
    Q_OBJECT
public:
    explicit Messages(QString accessToken, QObject *parent = nullptr);
    void sendMessage(int peerID, QString message, int messageIDs = -1);
    QByteArray getAttachesHistory(int chatID, QString mediaType, QString timeStart = QString::number(QDateTime::currentDateTime().toTime_t()));
    QByteArray getById(int messageID);

private:
    QString _accessToken;
    UrlSimplifier *requester;
    QString basicUrl;

    QByteArray sendMethod(QString methodName, QMap<QString, QString> params);


signals:

public slots:
};

#endif // MESSAGES_H
