#ifndef USER_H
#define USER_H

#include <QObject>

class User : public QObject
{
    Q_OBJECT
public:
    explicit User(QString accessToken, QObject *parent = nullptr);

private:
    QString _accessToken;

signals:

public slots:
};

#endif // USER_H
