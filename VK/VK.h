#ifndef VK_H
#define VK_H

#include <QObject>
#include "MessageListener.h"
#include "User.h"
#include "Messages.h"
#include "support/UrlSimplifier.h"

class VK : public QObject
{
    Q_OBJECT
public:
    explicit VK(QString accessToken, QObject *parent = nullptr);
    MessageListener *getMessageListener();
    Messages *getMessages();

private:
    QString _accessToken;

    MessageListener *messageListener;
    User *user;
    Messages *messages;
    UrlSimplifier *urlSimplifier;

signals:

public slots:
};

#endif // VK_H
