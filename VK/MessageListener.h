#ifndef MESSAGELISTENER_H
#define MESSAGELISTENER_H

#include <QObject>
#include "support/UrlSimplifier.h"
#include "json/json.h"

class MessageListener : public QObject
{
    Q_OBJECT
public:
    explicit MessageListener(QString accessToken, QObject *parent = nullptr);
    void startLongPollServer();
    void stopLongPollServer();
    bool isWorking();

private:
    QString _accessToken;
    UrlSimplifier *longPollRequester; //точка доступа к лонг полл серверу
    bool working;

    void parseEvents(QVariantMap &responseMap); //обработка событий

    QString _key;

    void processGetMessage(QVariantList variant);
    void processSmbdReadMsg(QVariantList variant);
    void processFriendOnline(QVariantList variant);
    void processFriendOffline(QVariantList variant);
    void processSmbdWriting(QVariantList variant);
    void processSmbdWritingConf(QVariantList variant);

    QVector<int> getMsgFlags(int sum);

signals:
    void eventGetMessage(int, QVector<int>, int, QString, QStringList); //messageID, messageFlags, messagePeer, messageText, messageAttachements
    void eventSmbdReadMsg(int, int);
    void eventFriendOnline(int);
    void eventFriendOffline(int);
    void eventSmbdWriting(int);
    void eventSmbdWritingConf(int);

private slots:
    void loopLongPollServer(QByteArray response); //зацикливает лонг полл

};

#endif // MESSAGELISTENER_H
