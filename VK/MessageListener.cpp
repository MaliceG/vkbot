#include "MessageListener.h"

MessageListener::MessageListener(QString accessToken, QObject *parent) : QObject(parent)
{
    _accessToken = accessToken;
    longPollRequester = new UrlSimplifier("", this);
    working = false;
}

void MessageListener::startLongPollServer()
{
    //получаю базовую информацию для зацикливания Long Poll
    longPollRequester->setUrl("https://api.vk.com/method/messages.getLongPollServer");
    longPollRequester->setQueryData("access_token", _accessToken);
    longPollRequester->setQueryData("lp_version", "2");
    longPollRequester->setQueryData("need_pts", "0");
    longPollRequester->setQueryData("v", "5.73");
    QtJson::JsonObject parsedArray = QtJson::parse(longPollRequester->get()).toMap();
    QVariantMap response = parsedArray["response"].toMap();

    //запускаю зацикливание Long Poll
    longPollRequester->clearQueryData();
    longPollRequester->setUrl("https://" + response["server"].toString());
    longPollRequester->setQueryData("act", "a_check");
    longPollRequester->setQueryData("key", response["key"].toString());
    longPollRequester->setQueryData("ts", response["ts"].toString());
    longPollRequester->setQueryData("wait", "25");
    longPollRequester->setQueryData("mode", "10");
    longPollRequester->setQueryData("version", "2");
    longPollRequester->getNoLock();
    connect(longPollRequester, SIGNAL(finished(QByteArray)), this, SLOT(loopLongPollServer(QByteArray)));
    working = true;
}

void MessageListener::stopLongPollServer()
{
    disconnect(longPollRequester, SIGNAL(finished(QByteArray)), this, SLOT(loopLongPollServer(QByteArray)));
    working = false;
}

bool MessageListener::isWorking()
{
    return working;
}

void MessageListener::loopLongPollServer(QByteArray response)
{
    QtJson::JsonObject parsedResponse = QtJson::parse(response).toMap();
    parseEvents(parsedResponse); //обрабатываю евенты

    //обновляю ts и запускаю лонг полл сервер
    longPollRequester->setQueryData("ts", parsedResponse["ts"].toString());
    longPollRequester->getNoLock();

}

void MessageListener::parseEvents(QVariantMap &responseMap)
{
    //TODO: расписать сигналы для эвентов, или хотя бы для полученных сообщений
//    qDebug() << "Response map: " << responseMap;
    QVariantList updatesList = responseMap["updates"].toList();
    if (!updatesList.isEmpty())
    {
        foreach (QVariant currentUpdate, updatesList)
        {
            QVariantList currentUpdateList = currentUpdate.toList();
//            qDebug() << "Current update: " << currentUpdateList;
            switch (currentUpdateList[0].toInt())
            {
                case 4: //Добавление нового сообщения.
                    processGetMessage(currentUpdateList);
                    break;

                case 7: //Прочтение всех исходящих сообщений с $peer_id вплоть до $local_id включительно.
                    processSmbdReadMsg(currentUpdateList);
                    break;

                case 8: //Друг $user_id стал онлайн
                    processFriendOnline(currentUpdateList);
                    break;

                case 9: //Друг $user_id стал оффлайн
                    processFriendOffline(currentUpdateList);
                    break;

                case 61: //Пользователь $user_id начал набирать текст в диалоге
                    processSmbdWriting(currentUpdateList);
                    break;

                case 62: //Пользователь $user_id начал набирать текст в конференции
                    processSmbdWritingConf(currentUpdateList);
                    break;

                default:
                    qDebug() << "NOT LISTED EVENT NUMBER: " << currentUpdateList[0].toInt();
            }
        }
    }
}

void MessageListener::processGetMessage(QVariantList variant)
{
//    не обрабатывает пересланные сообщения
    qDebug() << "Get new msg: " << variant;

    int messageID = variant[1].toInt();
    QVector<int> messageFlags = getMsgFlags(variant[2].toInt());
    int messagePeer = variant[3].toInt();
    QString messageText = variant[5].toString();
    QMap<QString, QVariant> attachementsMap = variant[6].toMap();

    //преобразование вложений в формат type;id
    QStringList attachesKeys = attachementsMap.keys();
    QStringList attachements;
    for (int i = 1; i <= attachesKeys.count() / 2; i++ )
        attachements.append(
                                attachementsMap.value("attach" + QString::number(i) + "_type").toString() + ";" +
                                attachementsMap.value("attach" + QString::number(i)).toString()
                            );

    emit eventGetMessage(messageID, messageFlags, messagePeer, messageText, attachements);
}

void MessageListener::processSmbdReadMsg(QVariantList variant)
{
//    qDebug() << "Somebody read message: " << variant;

    emit eventSmbdReadMsg(variant[1].toInt(), variant[2].toInt()); //peer_id, local_id
}

void MessageListener::processFriendOnline(QVariantList variant)
{
//    qDebug() << "Somebody online: " << variant;

    emit eventFriendOnline(-variant[1].toInt()); //user_id
}

void MessageListener::processFriendOffline(QVariantList variant)
{
//    qDebug() << "Somebody offline: " << variant;

    emit eventFriendOffline(-variant[1].toInt()); //user_id
}

void MessageListener::processSmbdWriting(QVariantList variant)
{
//    qDebug() << "Somebody writing msg: " << variant;

    emit eventSmbdWriting(variant[1].toInt()); //user_id
}

void MessageListener::processSmbdWritingConf(QVariantList variant)
{
//    qDebug() << "Somebody writing msg in conf: " << variant;

    emit eventSmbdWritingConf(variant[1].toInt()); //user_id
}

QVector<int> MessageListener::getMsgFlags(int sum)
{
    QVector<int> flags;

    int max = 1024; //флаги начинаются с 512 и заканчиваются 1
    //все значеняия флагов смотреть тут(https://vk.com/dev/using_longpoll_2?f=4.%20Флаги%20сообщений)

    while (max != 1)
    {
        max /= 2;
        if (sum - max >= 0)
        {
            flags.append(max);
            sum -= max;
        }
    }

    return flags;
}
