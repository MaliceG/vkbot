#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "VK/VK.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    VK *VKClient;

    void sendHelp(int messagePeer);
    void findArt(int messageID, int messagePeer, QStringList messageAttach);
    void sendUnixTime(int messageID, int messagePeer, QString messageText);

    bool isPhotoTheSame(QString url1, QString url2);
    template <typename T> T sqr(T x){return x * x;}

private slots:
    void switchServer();
    void processMessage(int messageID, QVector<int> messageFlags, int messagePeer, QString messageText, QStringList messageAttachs);

};

#endif // MAINWINDOW_H
